import React, { Component } from 'react';
import Register from "./Register.js";
import Login from "./Login";
import Dashboard from "./Dashboard";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      activeView: "registration-view"
    }
  }

  componentWillMount() {
    this.setState({ activeView: "registration-view" })
  }

  route(view) {
    this.setState({ activeView:"login-view"})
  }

  render() {
    return (
      <div>
        {this.state.activeView === "registration-view" ? 
          <Register route={this.route.bind(this)} />
        : null}
        {this.state.activeView === "login-view" ? 
          <Login route={this.route.bind(this)} />
        : null}
        {this.state.activeView === "dashboard-view" ? 
          <Dashboard />
        : null}
      </div>
    );
  }
}

export default App;
