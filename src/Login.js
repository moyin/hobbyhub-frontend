import React, {Component} from "react";

class Login extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: false,
			error: false,
			errorMessage: false
		}
	}

	handleSubmit(e) {
		e.preventDefault();

		if(this.state.loading) {
			return;
		}

		this.setState({ loading: true, error: false, errorMessage: false });

		let elements = e.target.elements;
		let isValid = true;

		for(var i = 0, len = elements.length; i < len; i++) {
			if(elements[i].value === "") {
				isValid = false;
				this.setState({ loading: false, error: true, errorMessage: `Please enter ${elements[i].name}` });
				break;
			} else {
				continue;
			}
		}

		if(!isValid) {
			return; 
		} else {
			this.setState({ loading: false });
			this.props.route("dashboard-view");
		}
	}

	render() {
		let loadingState = this.state.loading ? " loading" : "";
		let buttonLoadingText = this.state.loading ? "Connecting..." : "login";
		let errorState = this.state.error ? " active" : "";

		return(
			<div>
				<form className="def-form reg-form" onSubmit={this.handleSubmit.bind(this)}>
					<label className="header">Log In</label>
					<p className={"error" + errorState}>
                        <strong>Whoops!</strong> {this.state.errorMessage}</p>
					<input type="email" name="email" className="text-field" placeholder="Email Address" />
					<input type="password" name="password" className="text-field" placeholder="Password" />
					<input type="submit" name="submit" className={"def-btn" + loadingState} value={buttonLoadingText} />
				</form>
			</div>
		);
	}
}

export default Login;